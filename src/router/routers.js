// <reference path="../locale/lang/zh-TW.js" />
import Main from '@/components/main'
import parentView from '@/components/parent-view'

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面在切换标签后不会缓存，如果需要缓存，无需设置这个字段，而且需要设置页面组件name属性和路由配置的name一致
 *  //access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 * }
 */

export default [
    // {
    //   path: '/',
    //   name: 'Index',
    //   redirect: '/home',
    //   component: Main,
    //   meta: {
    //     hideInMenu: true,
    //     notCache: true
    //   },
    //   component: () => import('@/view/login/login.vue')
    // },
    // {
    //   path: '/transition',
    //   name: 'transition',
    //   meta: {
    //     title: 'Login',
    //     hideInMenu: true
    //   },
    //   component: () => import('@/view/login/transition.vue')
    // },
    {
        path: '/login',
        name: 'login',
        meta: {
            title: '登录 - Login',
            hideInMenu: true
        },
        component: () =>
            import ('@/view/login/login.vue')
    },
    {
        path: '/dashboard',
        name: '_home',
        redirect: '/home',
        component: Main,
        meta: {
            hideInMenu: true,
            notCache: true
        },
        children: [{
            path: '/home',
            name: 'home',
            meta: {
                hideInMenu: true,
                title: '首页',
                notCache: true,
                icon: 'iconfont iconshouye'
            },
            component: () =>
                import ('@/view/single-page/home')
        }]
    },

    {
        path: '/member',
        name: 'member',
        meta: {
            icon: 'iconfont icon03',
            showAlways: true,
            title: '会员中心'
        },
        component: Main,
        children: [{
                path: 'viplist',
                name: 'viplist',
                meta: {
                    icon: 'iconfont iconweixinhuiyuan',
                    title: '会员列表',
                    keepAlive: true
                },
                component: () =>
                    import ('@/view/member/viplist.vue')
            },

            {
                path: 'viplistex',
                name: 'viplistex',
                meta: {
                    icon: 'iconfont iconweixinhuiyuan',
                    title: '会员列表高级',
                    keepAlive: true
                },
                component: () =>
                    import ('@/view/member/viplistex.vue')
            },
            {
                path: 'friendlist',
                name: 'friendlist',
                meta: {
                    icon: 'iconfont iconweixinhuiyuan',
                    title: '企微好友'
                },
                component: () =>
                    import ('@/view/member/friendlist.vue')
            },
            {
                path: 'vipinfo',
                name: 'vipinfo',
                meta: {
                    icon: 'md-funnel',
                    hideInMenu: true,
                    title: '客户详情',
                },
                component: () =>
                    import ('@/view/member/vipinfo.vue')
            },
            {
                path: 'viplisttag',
                name: 'viplisttag',
                meta: {
                    icon: 'iconfont iconweixinhuiyuan',
                    title: '会员查询',
                    hideInMenu: true,
                },
                component: () =>
                    import ('@/view/member/viplisttag.vue')
            },

        ]
    },
    {
        path: '/marketing',
        name: 'marketing',
        meta: {
            icon: 'iconfont iconhulianwangyingxiao-',
            title: '营销中心'
        },
        component: Main,
        children: [{
                path: 'actadd', //'''marketing_card/actadd',
                name: 'actadd',
                meta: {
                    icon: 'iconfont iconyingxiaowanfa',
                    title: '创建活动',
                },
                component: () =>
                    import ('@/view/marketing/actadd.vue')
            },
            {
                path: 'actaddsel',
                name: 'actaddsel',
                meta: {
                    hideInMenu: true,
                    icon: 'iconfont iconyingxiaowanfa',
                    title: '去赠券'
                },
                component: () =>
                    import ('@/view/marketing/actaddsel.vue')
            },
            {
                path: 'actaddgift',
                name: 'actaddgift',
                meta: {
                    hideInMenu: true,
                    icon: 'iconfont iconyingxiaowanfa',
                    title: '去投放'
                },
                component: () =>
                    import ('@/view/marketing/actaddgift.vue')
            },
            {
                path: 'actaddhome',
                name: 'actaddhome',
                meta: {
                    hideInMenu: true,
                    icon: 'iconfont iconyingxiaowanfa',
                    title: '精准投放'
                },
                component: () =>
                    import ('@/view/marketing/actaddhome.vue')
            },
            {
                path: 'actlist',
                name: 'actlist',
                meta: {
                    icon: 'iconfont icon-icon',
                    title: '活动记录',
                    keepAlive: true
                },
                component: () =>
                    import ('@/view/marketing/actlist.vue')
            },
            {
                path: 'actshow',
                name: 'actshow',
                meta: {
                    icon: 'iconfont icon-icon',
                    title: '活动详情',
                    hideInMenu: true,
                },
                component: () =>
                    import ('@/view/marketing/actshow.vue')
            },
            {
                path: 'acthome',
                name: 'acthome',
                meta: {
                    icon: 'iconfont iconyingxiaowanfa',
                    title: '精准营销'
                },
                component: () =>
                    import ('@/view/marketing/acthome.vue')
            },
            {
                path: 'giftdt',
                name: 'giftdt',
                meta: {
                    icon: 'iconfont icon-jiaoyijilu',
                    title: '领取记录',
                    notCache: true
                },
                component: () =>
                    import ('@/view/marketing/giftdt.vue')
            },
            {
                path: 'giftlist',
                name: 'giftlist',
                meta: {
                    icon: 'iconfont icon-kucunguanli-',
                    title: '卡券库'
                },
                component: () =>
                    import ('@/view/marketing/giftlist.vue')
            },
            {
                path: 'giftadd',
                name: 'giftadd',
                meta: {
                    icon: 'md-funnel',
                    hideInMenu: true,
                    title: '新增卡券'
                },
                component: () =>
                    import ('@/view/marketing/giftadd.vue')
            },
            {
                path: 'giftshow',
                name: 'giftshow',
                meta: {
                    icon: 'md-funnel',
                    hideInMenu: true,
                    title: '卡券详情'
                },
                component: () =>
                    import ('@/view/marketing/giftshow.vue')
            },
            {
                path: 'wxsms',
                name: 'wxsms',
                meta: {
                    icon: 'iconfont icon-weixin',
                    showAlways: true,
                    title: '微信营销'
                },
                component: parentView,
                children: [{
                        path: 'wxsend',
                        name: 'wxsend',
                        meta: {
                            icon: 'iconfont icon-icon',
                            title: '发送记录'
                        },
                        component: () =>
                            import ('@/view/marketing/wxsend.vue')
                    },
                    {
                        path: 'wxdraft',
                        name: 'wxdraft',
                        meta: {
                            icon: 'iconfont icon-tuwenxiangqing',
                            title: '图文草稿'
                        },
                        component: () =>
                            import ('@/view/marketing/wxdraft.vue')
                    },
                    {
                        path: 'wxdraftadd',
                        name: 'wxdraftadd',
                        meta: {
                            hideInMenu: true,
                            title: '图文草稿详情'
                        },
                        component: () =>
                            import ('@/view/marketing/wxdraftadd.vue')
                    },
                    {
                        path: 'wxnews',
                        name: 'wxnews',
                        meta: {
                            icon: 'iconfont icon-tuwenxiangqing',
                            title: '历史图文'
                        },
                        component: () =>
                            import ('@/view/marketing/wxnews.vue')
                    },
                    {
                        path: 'wxnewsadd',
                        name: 'wxnewsadd',
                        meta: {
                            hideInMenu: true,
                            title: '历史图文详情'
                        },
                        component: () =>
                            import ('@/view/marketing/wxnewsadd.vue')
                    },
                    {
                        path: 'wximages',
                        name: 'wximages',
                        meta: {
                            icon: 'iconfont icon-tupian',
                            title: '图片库'
                        },
                        component: () =>
                            import ('@/view/marketing/wximages.vue')
                    },
                    {
                        path: 'wxsendadd',
                        name: 'wxsendadd',
                        meta: {
                            icon: 'iconfont icon-kucunguanli-',
                            title: '图文群发',
                            hideInMenu: true
                        },
                        component: () =>
                            import ('@/view/marketing/wxsendadd.vue')
                    }
                ]
            }
        ]
    },
    {
        path: '/mngwdg',
        name: 'mngwdg',
        meta: {
            icon: 'iconfont icondaogou1',
            title: '微导购'
        },
        component: Main,
        children: [{
                path: 'wdgdept',
                name: 'wdgdept',
                meta: {
                    icon: 'iconfont icon-mendianxinxi',
                    title: '门店管理'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgdept.vue')
            },
            {
                path: 'wdgemp',
                name: 'wdgemp',
                meta: {
                    icon: 'iconfont icon-chengyuanguanli',
                    title: '成员管理'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgemp.vue')
            },
            {
                path: 'wdgparm',
                name: 'wdgparm',
                meta: {
                    icon: 'iconfont iconcanshupeizhi',
                    title: '参数配置'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgparm.vue')
            },
            {
                path: 'wdgparmset',
                name: 'wdgparmset',
                meta: {
                    icon: 'iconfont iconcanshupeizhi',
                    title: '模板配置',
                    hideInMenu: true
                },
                component: () =>
                    import ('@/view/mngwdg/wdgparmset.vue')
            },
            {
                path: 'wdggift',
                name: 'wdggift',
                meta: {
                    icon: 'iconfont iconyejifenxi',
                    title: '导购赠券'
                },
                component: () =>
                    import ('@/view/mngwdg/wdggift.vue')
            },

        ]
    },
    {
        path: '/wdgtask',
        name: 'wdgtask',
        meta: {
            icon: 'iconfont iconduanxin1',
            title: '导购任务'
        },
        component: Main,
        children: [{
                path: 'signPre',
                name: 'signPre',
                meta: {
                    icon: 'iconfont iconshengri',
                    title: '会员注册赠券'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgtask.vue')
            },
            {
                path: 'collectPre',
                name: 'collectPre',
                meta: {
                    icon: 'iconfont iconshengri',
                    title: '商城收藏赠券'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgtask.vue')
            },
            {
                path: 'addcartPre',
                name: 'addcartPre',
                meta: {
                    icon: 'iconfont iconshengri',
                    title: '商城加购赠券'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgtask.vue')
            },
            {
                path: 'seekPre',
                name: 'seekPre',
                meta: {
                    icon: 'iconfont iconshengri',
                    title: '商城搜索赠券'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgtask.vue')
            },
            {
                path: 'minute',
                name: 'minute',
                meta: {
                    icon: 'iconfont iconshengri',
                    title: '商城购买赠券'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgtask.vue')
            },
            {
                path: 'birthday',
                name: 'birthday',
                meta: {
                    icon: 'iconfont iconshengri',
                    title: '会员生日赠券'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgtask.vue')
            },
            {
                path: 'monthDay',
                name: 'monthDay',
                meta: {
                    icon: 'iconfont iconmanyueli',
                    title: '购物满月赠券'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgtask.vue')
            },
            {
                path: 'hundredDay',
                name: 'hundredDay',
                meta: {
                    icon: 'iconfont iconbairili',
                    title: '购物百日赠券'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgtask.vue')
            },
            {
                path: 'yearDay',
                name: 'yearDay',
                meta: {
                    icon: 'iconfont icon365',
                    title: '购物周年赠券'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgtask.vue')
            },
            {
                path: 'dormant',
                name: 'dormant',
                meta: {
                    icon: 'iconfont iconxiumian1',
                    title: '定时休眠激活'
                },
                component: () =>
                    import ('@/view/mngwdg/wdgtask.vue')
            }
        ]
    },
    {
        path: '/tags',
        name: 'tags',
        meta: {
            icon: 'iconfont iconbiaoqian',
            showAlways: true,
            title: '标签引擎'
        },
        component: Main,
        children: [{
                path: 'tagauto',
                name: 'tagauto',
                meta: {
                    icon: 'iconfont icon-weixingongzhonghao',
                    title: '自动标签'
                },
                component: () =>
                    import ('@/view/member/tagauto.vue')
            },
            {
                path: 'tagall',
                name: 'tagall',
                meta: {
                    icon: 'iconfont icon-qiye',
                    title: '导购标签'
                },
                component: () =>
                    import ('@/view/member/tagall.vue')
            }
        ]
    },
    {
        path: '/mngbi',
        name: 'mngbi',
        meta: {
            icon: 'iconfont iconshujufenxi',
            showAlways: true,
            title: '数据分析'
        },
        component: Main,
        children: [{
                path: 'buyRanking',
                name: 'buyRanking',
                meta: {
                    icon: 'iconfont iconfugoushuai',
                    title: '复购排名分析'
                },
                component: () =>
                    import ('@/view/mngbi/buyRanking.vue')
            },
            {
                path: 'vipConsume',
                name: 'vipConsume',
                meta: {
                    icon: 'iconfont iconxiaofei',
                    title: '会员消费分析'
                },
                component: () =>
                    import ('@/view/mngbi/vipConsume.vue')
            },
            {
                path: 'vipLV',
                name: 'vipLV',
                meta: {
                    icon: 'iconfont icondengjizhanbiicon',
                    title: '会员等级占比'
                },
                component: () =>
                    import ('@/view/mngbi/vipLV.vue')
            },
            {
                path: 'memberAge',
                name: 'memberAge',
                meta: {
                    icon: 'iconfont iconnianling',
                    title: '会员年龄分析'
                },
                component: () =>
                    import ('@/view/mngbi/memberAge.vue')
            },
            {
                path: 'vipCoupon',
                name: 'vipCoupon',
                meta: {
                    icon: 'iconfont iconnianling',
                    title: 'VIP券报表'
                },
                component: () =>
                    import ('@/view/mngbi/vipCoupon.vue')
            },
            {
                path: 'vipBirthday',
                name: 'vipBirthday',
                meta: {
                    icon: 'iconfont iconnianling',
                    title: 'VIP生日提醒'
                },
                component: () =>
                    import ('@/view/mngbi/vipBirthday.vue')
            },
            {
                path: 'retail',
                name: 'retail',
                meta: {
                    icon: 'iconfont icon-rfmjiaochafenxi',
                    title: '零售品类'
                },
                component: () =>
                    import ('@/view/mngbi/retail.vue')
            },
            {
                path: 'sale_ranking',
                name: 'sale_ranking',
                meta: {
                    icon: 'iconfont icon-rfmjiaochafenxi',
                    title: '畅销款排行'
                },
                component: () =>
                    import ('@/view/mngbi/sale_ranking.vue')
            },
            {
                path: 'repertory',
                name: 'repertory',
                meta: {
                    icon: 'iconfont icon-rfmjiaochafenxi',
                    title: '库存分析'
                },
                component: () =>
                    import ('@/view/mngbi/repertory.vue')
            }
        ]
    },
    {
        path: '/settings',
        name: 'settings',
        meta: {
            icon: 'iconfont iconxitongshezhi',
            showAlways: true,
            title: '系统设置'
        },
        component: Main,
        children: [{
                path: 'crmconfig',
                name: 'crmconfig',
                meta: {
                    icon: 'iconfont iconshouquan',
                    title: '企业微信授权'
                },
                component: () =>
                    import ('@/view/mngwdg/crmconfig.vue')
            },
            {
                path: 'wxconfig',
                name: 'wxconfig',
                meta: {
                    icon: 'iconfont icongongzhonghaoshouquan',
                    title: '公众号授权'
                },
                component: () =>
                    import ('@/view/mngwdg/wxconfig.vue')
            },
            {
                path: 'wxmemu',
                name: 'wxmemu',
                meta: {
                    icon: 'iconfont icongongzhonghaocaidanpeizhi',
                    title: '公众号菜单'
                },
                component: () =>
                    import ('@/view/mngwdg/wxmenu.vue')
            }
        ]
    },
    {
        path: '/401',
        name: 'error_401',
        meta: {
            hideInMenu: true
        },
        component: () =>
            import ('@/view/error-page/401.vue')
    },
    {
        path: '/500',
        name: 'error_500',
        meta: {
            hideInMenu: true
        },
        component: () =>
            import ('@/view/error-page/500.vue')
    },
    {
        path: '*',
        name: 'error_404',
        meta: {
            hideInMenu: true
        },
        component: () =>
            import ('@/view/error-page/404.vue')
    },
    {
        path: '/image-textCard',
        name: 'image-textCard',
        meta: {
            hideInMenu: true
        },
        component: () =>
            import ('@/view/image-textCard/image-textCard.vue')
    },



    // 365部分
    {
        path: '/caigou',
        name: 'caigou',
        meta: {
            icon: 'iconfont icon03',
            showAlways: true,
            title: '采购' //会员中心
        },
        component: Main,
        children: [{
                path: 'caigoudanju',
                name: 'caigoudanju',
                meta: {
                    icon: 'iconfont iconweixinhuiyuan',
                    title: '采购单据', //会员列表
                    // keepAlive: true,
                    showAlways: true,

                },
                component: parentView,
                children: [{
                            path: 'caigoudan',
                            name: 'caigoudan',
                            meta: {
                                icon: 'iconfont icon-icon',
                                title: '采购单'
                            },
                            component: () =>
                                import ('@/view/caigou/caigoudanju/caigoudan.vue')
                        },
                        {
                            path: 'tuihuodan',
                            name: 'tuihuodan',
                            meta: {
                                icon: 'iconfont icon-tuwenxiangqing',
                                title: '采购退货单'
                            },
                            component: () =>
                                import ('@/view/caigou/caigoudanju/tuihuodan.vue')
                        },
                        {
                            path: 'xinzengcaigou',
                            name: 'xinzengcaigou',
                            meta: {
                                icon: 'md-funnel',
                                hideInMenu: true,
                                title: '新增采购单',
                            },
                            component: () =>
                                import ('@/view/caigou/caigoudanju/xinzengcaigou.vue')
                        }, {
                            path: 'caigoutui',
                            name: 'caigoutui',
                            meta: {
                                icon: 'md-funnel',
                                hideInMenu: true,
                                title: '新增采购退货单',
                            },
                            component: () =>
                                import ('@/view/caigou/caigoudanju/caigoutui.vue')
                        },
                    ]
                    // component: () => import('@/view/member/viplist.vue')
            },
            {
                path: 'gongyingshang',
                name: 'gongyingshang',
                meta: {
                    icon: 'iconfont iconweixinhuiyuan',
                    title: '供应商',
                    keepAlive: true,
                    showAlways: true,

                },
                component: parentView,
                children: [{
                        path: 'gongying',
                        name: 'gongying',
                        meta: {
                            icon: 'iconfont icon-icon',
                            title: '供应商档案'
                        },
                        component: () =>
                            import ('@/view/caigou/gongyingshang/gongying.vue')
                    }, {
                        path: 'xinzenggongying',
                        name: 'xinzenggongying',
                        meta: {
                            icon: 'md-funnel',
                            hideInMenu: true,
                            title: '新增供应商',
                        },
                        component: () =>
                            import ('@/view/caigou/gongyingshang/xinzenggongying.vue')
                    }, ]
                    // component: () => import('@/view/member/viplistex.vue')
            }
        ]
    },
    {
        path: '/shangpin',
        name: 'shangpin',
        meta: {
            icon: 'iconfont iconhulianwangyingxiao-',
            showAlways: true,
            title: '商品' //会员中心
        },
        component: Main,
        children: [{
            path: 'shangpinguanli',
            name: 'shangpinguanli',
            meta: {
                icon: 'iconfont iconyingxiaowanfa',
                title: '商品管理', //会员列表
                // keepAlive: true,
                showAlways: true,

            },
            component: parentView,
            children: [{
                    path: 'shangpindangan',
                    name: 'shangpindangan',
                    meta: {
                        icon: 'iconfont iconyingxiaowanfa',
                        title: '商品档案'
                    },
                    component: () =>
                        import ('@/view/shangpin/shangpinguanli/shangpindangan.vue')
                }, ]
                // component: () => import('@/view/member/viplist.vue')
        }, ]
    },
]