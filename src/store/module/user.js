import { promise } from '@/api/user'
import { getToken, setToken } from '@/libs/util'
export default {
  state: {
    token: getToken(),
  },
  actions: {
    crm_getdata ({ commit }, data) {
      return promise(data)
    },
    // 退出登录
    get_logout ({ state, commit }) {
      return new Promise((resolve, reject) => {
        setToken('')
        resolve()
      })
    },
    ShowWarning ({ commit }, msg) {
      this._vm.$Notice.warning({
        title: '系统提醒',
        desc: msg
      })
    },
    ShowMessage ({ commit }, msg) {
      this._vm.$Notice.success({
        title: '系统通知',
        desc: msg
      })
    },
    ShowWarningTop ({ commit }, msg) {
      this._vm.$Message.warning({
        content: msg,
        duration: 5
      });
    },
    ShowMessageTop ({ commit }, msg) {
      this._vm.$Message.success({
        content: msg,
        duration: 5
      });
    },
    crm_bistorelist({commit}){
      return promise({
          api : "crm_bistorelist.t",
          cache : 10
      })
    },
    crm_company ({ commit }) {
      return promise({
        api:'crm_company.t',
        tolow:0,
        cache:30
      })
    },
    crm_getgiftlist ({ commit }, data) {
      data.api = 'crm_getgiftlist2.t3'   
      return promise(data)
    },
    // Close ({ commit }, route) {      
    //   console.log(route)
    //   this._vm.$nextTick(() => {
    //     this.closeTag(route)
    //   })
    // },
  }
}
