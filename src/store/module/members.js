
import { postdata, promise } from '@/api/user'

export default {
  state: {
    levels: [],
    boards: [],
    tags: [],
    labels: [],
    follows:[],
    servants: [],
    vips: []
  },
  mutations: {
    setLevels (state, levels) {
      state.levels = levels
    },
    setBoards (state, boards) {
      state.boards = boards
    },
    setLabels (state, labels) {
      state.labels = labels
    },
    setFollows (state, follows) {
      state.follows = follows
    },
    setServants (state, servants) {
      state.servants = servants
    },
    setVips (state, vips) {
      state.vips = vips
    }
  },
  getters: {
    levels: state => state.levels,
    boards: state => state.boards,
    tags: state => state.tags,
    labels: state => state.labels,
    follows: state => state.follows,
    servants: state => state.servants,
    vips: state => state.vips,
  },
  actions: {
    crm_getAutoTag({ commit }){
      return new Promise((resolve, reject) => {
        postdata({
          api: 'crm_getAutoTag.t3',
          page:1, 
          pagesize:1000 
        }).then(res => {
          const data = res.data.data;          
          commit('setLabels', data)
          resolve();
        }).catch(err => {
          reject(err)
        })
      })
    },
    crm_getAutoTagAll({ commit },data){
      data.api = 'crm_getAutoTag.t3'
      data.showall = 1
      return promise(data)
    },
    crm_getviplistex({ commit },entry){   
      entry.api = 'crm_getviplistex.t3'
      return promise(entry)
    },
    crm_getvipkey({ commit },entry) { 
      entry.api = 'crm_getvipkey.t'
      entry.tolow = 0
      return promise(entry)
    },

    crm_getGiftStyle({ commit },data){  //获取已上线的商品列表
      data.api = "crm_getGiftStyle.t2"
      data.tolow = 0
      return promise(data)
    },
    crm_getstorename({commit},data){
      data.api = "crm_getstorename.s"  
      return promise(data)
    },
    crm_getstylename({commit},data){
      data.api = "crm_getstylename.s"  
      return promise(data)
    },
    //获取商品类目
    crm_getGiftStyleAttr({ commit }){  //获取已上线的门店列表
      return promise({
        api : "crm_getGiftStyleAttr.t2",
        cache : 30,
        tolow : 0
      })      
    },
    crm_getactlist({ commit },data){
      data.api = "crm_getactlist.t3"  
      return promise(data)      
    },
    crm_actdel({commit},data){      
      data.api = "crm_actdel.n"
      return promise(data)  
    },


  }
}