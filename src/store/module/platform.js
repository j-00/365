
import { promise } from '@/api/user'

export default {
  state: {
    auth_items: [],
    auth_wechat:[],
    auth_entry: {},
    grant_url: '',
    store: {},
    areas: [],
    storeList: [],
    storeCount: 0,
    guides: []
  },
  mutations: {
    setAccess (state, access) {
      state.access = access
    },
    setAuthItems (state, items) {
      state.auth_items = items
    },
    setAuth_wechat(state, items){
      state.auth_wechat = items
    },
    setAuthEntry (state, entry) {
      state.auth_entry = entry
    },
    setGrantUrl (state, url) {
      state.grant_url = url
    },
    setAreas (state, areas) {
      state.areas = areas
    },
    setStoreList (state, storeList) {
      state.storeList = storeList
    },
    setStoreCount (state, count) {
      state.storeCount = count
    },
    setGuides (state, guides) {
      state.guides = guides
    }
  },
  getters: {
    auth_items: state => state.auth_items,
    auth_wechat : state => state.auth_wechat,
    auth_entry: state => state.auth_entry,     
    grant_url: state => state.grant_url,
    areas: state => state.areas,
    storeList: state => state.storeList,
    storeCount: state => state.storeCount,
    guides: state => state.guides,
  },
  actions: {
    crm_getStoreFlag({ commit }){
      return promise({
        api:"crm_getStoreFlag.t",
        cache:30
      })
    },
    crm_getStoreType({commit}) {
      return promise({
        api : "crm_getStoreType.t2",
        cache : 30,
        tolow : 0 
      })     
    },
    crm_getVipType({commit}) {
      return promise({
        api:"crm_getVipType.t",
        cache:30
      })
    },
    crm_getstorelist({ commit },{data}){
      data.api = "crm_getstorelist.t3"
      return promise(data)  
    },
    crm_taglist({ commit }){
      return promise({
        api:"crm_taglist.t2"
      })
    },
    //////////////////////////////
  }
}
