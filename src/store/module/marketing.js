import { promise } from '@/api/user'

export default {
  state: {
  },
  mutations: {
  },
  getters: {
    
  },
  actions: {
    crm_getstore({commit},data){
      data.api = 'crm_getstore.t'
      data.keyword=data.data
      data.tolow = '0' 
      return promise(data)
    },
    crm_getarea({commit}){
      return promise({
        api: 'crm_getarea.t',
        tolow: '0',
        cache : 30 
      })
    },
    crm_getemp({commit},data){
      data.api = 'crm_getemp.t'
      data.storeid = data.data
      data.tolow = '0' 
      return promise(data)
    },
    crm_materiallist ({ commit }, data) {
      data.api = 'crm_materiallist'
      return promise(data)      
    },
    ////////////////////    

  }
}
