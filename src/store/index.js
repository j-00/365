import Vue from 'vue'
import Vuex from 'vuex'

import user from './module/user'
import app from './module/app'
import platform from './module/platform'
import marketing from './module/marketing'
import members from './module/members'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    //
  },
  mutations: {
    //
  },
  actions: {
    //
  },
  modules: {
    app,
    user,    
    platform,
    marketing,
    members,
  }
})
