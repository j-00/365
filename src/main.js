import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'





import MetaInfo from 'vue-meta-info'
Vue.use(MetaInfo)

import '../static/alIcon/iconfont.css'
import '../static/bicon/iconfont.css'
import '../static/bicon1/iconfont.css'
import '../static/actIcon/iconfont.css'

import store from './store'
import iView from 'view-design'
// import i18n from '@/locale'
import config from '@/config'
import importDirective from '@/directive'
import {
    directive as clickOutside
} from 'v-click-outside-x'
import installPlugin from '@/plugin'
import './index.less'
import '@/assets/icons/iconfont.css'
import TreeTable from 'tree-table-vue'
import VOrgTree from 'v-org-tree'
import 'v-org-tree/dist/v-org-tree.css'
// echarts
import echarts from "echarts";
Vue.prototype.$ech = echarts;
Vue.prototype.axios = axios

Vue.config.productionTip = false

Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};

// 实际打包时应该不引入mock
//if (process.env.NODE_ENV !== 'production') require('@/mock')

Vue.use(iView)
Vue.prototype.$Notice.config({
    top: 50,
    duration: 10
});
Vue.use(TreeTable)
Vue.use(VOrgTree)
    /**
     * @description 注册admin内置插件
     */
installPlugin(Vue)
Vue.prototype.$config = config
    /**
     * 注册指令
     */
importDirective(Vue)
Vue.directive('clickOutside', clickOutside)

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
})