import axios from '@/libs/api.request'
import { getToken } from '@/libs/util'

export const postdata = (data) => {
  data.access_token = getToken()||''
  return axios.request({
      // url: 'route.ashx?api=' + data.api,
      url: 'webapi.ashx?api=' + data.api,
      method: 'post', 
      data: data
  })
}

export const promise = (data) => {
  data.access_token = getToken()||''
  return new Promise((resolve,reject) =>{
    axios.request({
      // url: 'route.ashx?api=' + data.api,
      url: 'webapi.ashx?api=' + data.api, //365
      method: 'post', 
      data: data
    }).then( res => {
      resolve(res.data)
    }).catch( err => {
      reject(err)
    })
  })  
}
